@extends('front.layout.app')

@section('title', 'Market')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <front-market></front-market>
        </div>
    </div>
@endsection
