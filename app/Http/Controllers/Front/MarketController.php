<?php

namespace Henkoh\Http\Controllers\Front;

use Illuminate\Http\Request;
use Henkoh\Http\Controllers\Controller;
use Henkoh\Market;

class MarketController extends Controller
{

    public $perPage = 10;
    
    public function index(Request $request){

    	if($request->ajax()){

    		if($request->has('search')){
                return Market::where('title','like', '%'.$request->search.'%')
                            ->where('status', 'Published')
                            ->orderBy('created_at', 'desc')
                            ->get();
                
            } else {

                if($request->has('page')){

                    $skip = intval(($request->page - 1) * 10);

                    return Market::where('status', 'Published')
                            ->orderBy('created_at', 'desc')
                            ->skip($skip)
                            ->take($this->perPage)
                            ->get();
                }

                $total  = Market::where('status','Published')->count();
    			$data   = Market::where('status', 'Published')
                            ->orderBy('created_at', 'desc')
                            ->take($this->perPage)
                            ->get();

                return ['total' => $total, 'data' => $data];
    		}
    	}

    	return view('front.market.index');
    }
}
