<?php

namespace Henkoh\Http\Controllers;

use Illuminate\Http\Request;
use Henkoh\Market;
use Illuminate\Support\Facades\DB;

// image upload
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MarketController extends Controller
{
    
    public function index(Request $request){

        if($request->ajax()){

            $rows   = Market::with('user')->orderBy('created_at', 'desc')->get();
            $setup  = DB::collection('market_setup')->first();

    		return [
                'rows'                  => $rows, 
                'setup'                 => $setup, 
                'totalPosts'            => $rows->count(), 
                'totalPostsQueue'       => $rows->where('status','Queue')->count(),
                'totalPostsPublished'   => $rows->where('status','Published')->count(),
                'totalPostsSold'        => $rows->where('status','Sold')->count()
            ];
    	}
    	return view('admin.market.index');
    }

    public function store(Request $request){

    	$validator = \Validator::make($request->all(), [
            'title' 	=> 'required|max:180',
            'category' 	=> 'required',
            'status' 	=> 'required',
            'content' 	=> 'required',
            'currency' 	=> 'required',
            'price' 	=> 'required',
            'images' 	=> 'required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

    	$market            = new Market;
    	$market->title     = $request->title;
    	$market->slug      = str_slug($request->title);
    	$market->category  = $request->category;
    	$market->status    = $request->status;
        $market->content   = $request->content;
    	$market->views     = 0;
    	$market->currency  = $request->currency;
    	$market->price 	   = $request->price;
    	$market->tags      = $request->tags;
        $market->images    = $request->images;
    	$market->user_id   = \Auth::user()->id;

    	$market->save();

    	return $market;
    }

    public function delete(Request $request){

    	if(Market::destroy($request->id)){

    		return 'success';
    	}

    	return response('Not Found',412);
    }

    public function update(Request $request){

    	$validator = \Validator::make($request->all(), [
            'title' 	=> 'required|max:180|unique:market,_id',
            'category' 	=> 'required',
            'status' 	=> 'required',
            'content' 	=> 'required',
            'currency' 	=> 'required',
            'price' 	=> 'required',
            'images' 	=> 'required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

    	$market = Market::find($request->id);

    	$market->title 		= $request->title;
    	// $market->slug 		= str_slug($request->title);
    	$market->category 	= $request->category;
    	$market->status 	= $request->status;
    	$market->content 	= $request->content;
    	$market->currency 	= $request->currency;
    	$market->price 		= $request->price;
    	$market->tags 		= $request->tags;
    	$market->images 	= $request->images;

    	$market->save();

    	return $request->images;
    }

    public function saveImages(Request $request){

    	// Setup 
    	$base 	= 'img';
    	$width 	= 800;
    	$height	= 600;


    	$urlPath 		= DIRECTORY_SEPARATOR . $base . DIRECTORY_SEPARATOR . 'market' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m');
		$absolutePath 	= public_path() . DIRECTORY_SEPARATOR . $urlPath;
        
        if(!File::isDirectory($absolutePath)){
            File::makeDirectory($absolutePath, 0755, true, true);
        }

		$imageName 			= date('Ymdhis') . '-' . STR::random(10) . '.' . $request->image->getClientOriginalExtension();
		$fullUrlPath 		= $urlPath . DIRECTORY_SEPARATOR . $imageName;
		$fullabsolutePath 	= $absolutePath . DIRECTORY_SEPARATOR . $imageName;

		$background 		= Image::canvas($width, $height);

		// but keep aspect-ratio and do not size up,
		$img = Image::make($request->file('image')->getRealPath())->fit($width, $height, function ($c) {
		    $c->aspectRatio();
		    // $c->upsize();
		});
		
		// insert resized image centered into background
		$background->insert($img, 'center');

		// la marca de agua
		// $background->insert('images/sales/watermark.png');

		// save or do whatever you like
		$background->save($fullabsolutePath);

        return $fullUrlPath;
    	
    }

    public function deleteImage(Request $request){

    	if($request->has('url')){

    		$path = public_path() . $request->url;
    		if(File::exists($path)){
    			
    			File::delete($path);
    			return 'Jevi';
    		}
    	}
    	
    	return response('Not Found',412);
    }
}
