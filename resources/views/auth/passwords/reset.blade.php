@extends('front.layout.lite')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>        
        <div>
            <h1 class="logo-name">.</h1>
        </div>
        <h3>Reset Password</h3>
        <form class="m-t" method="POST" role="form" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="Email" required="" name="email">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" required="" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation" required>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            
            <button type="submit" class="btn btn-primary block full-width m-b">Reset Password</button>

        </form>
        <p class="m-t"> <small>Henkoh 2015 - {{ date('Y') }}</small> </p>
    </div>
</div>

@endsection
