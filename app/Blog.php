<?php

namespace Henkoh;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Blog extends Eloquent
{
    protected $collection = 'blog';

    public function user()
    {
        return $this->belongsTo('Henkoh\User');
    }
}
