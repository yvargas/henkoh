@extends('admin.layout.app')

@section('title', 'Dashboard')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <admin-market></admin-market>
        </div>
    </div>
@endsection
