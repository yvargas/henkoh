<?php

namespace Henkoh\Http\Controllers;

use Illuminate\Http\Request;
use Henkoh\Blog;
use Illuminate\Support\Facades\DB;

// image upload
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    public function index(Request $request){

        if($request->ajax()){

            $rows   = Blog::with('user')->orderBy('created_at', 'desc')->get();
    		$setup	= DB::collection('blog_setup')->first();

    		return ['rows' => $rows, 'setup' => $setup];
    	}
    	return view('admin.blog.index');
    }

    public function store(Request $request){

    	$validator = \Validator::make($request->all(), [
            'title' 	=> 'required|max:180',
            'category' 	=> 'required',
            'status' 	=> 'required',
            'content' 	=> 'required',
            'cover' 	=> 'required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

    	$blog            = new Blog;
    	$blog->title     = $request->title;
        $blog->user_id   = \Auth::user()->id;
    	$blog->slug      = str_slug($request->title);
    	$blog->category  = $request->category;
    	$blog->status    = $request->status;
        $blog->content   = $request->content;
    	$blog->views     = 0;
    	$blog->tags      = $request->tags;
    	$blog->image     = $request->image;

    	$blog->save();

        $blog->user = $blog->user;

    	return $blog;
    }

    public function delete(Request $request){

    	if(Blog::destroy($request->id)){

    		return 'success';
    	}

    	return response('Not Found',412);
    }

    public function update(Request $request){

    	$validator = \Validator::make($request->all(), [
            'title' 	=> 'required|max:180|unique:blog,_id',
            'category' 	=> 'required',
            'status' 	=> 'required',
            'content' 	=> 'required',
            'cover'     => 'required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

    	$blog = Blog::find($request->id);

    	$blog->title       = $request->title;
    	$blog->category    = $request->category;
    	$blog->status      = $request->status;
    	$blog->content     = $request->content;
    	$blog->tags        = $request->tags;
    	$blog->image       = $request->image;

    	$blog->save();

    	return $request->images;
    }

    public function saveImages(Request $request){

    	// Setup 
    	$base 	    = 'img';
    	$width 	    = 850;
    	$height	    = 300;

        $tmbWidth   = 200;
        $tmbHeight  = 200;


    	$urlPath 		= DIRECTORY_SEPARATOR . $base . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR . date('Y') . DIRECTORY_SEPARATOR . date('m');
		$absolutePath 	= public_path() . DIRECTORY_SEPARATOR . $urlPath;
        
        if(!File::isDirectory($absolutePath)){
            File::makeDirectory($absolutePath, 0755, true, true);
        }

        $imgName             = date('Ymdhis') . '-' . STR::random(10);
        $imageName           = $imgName . '.' . $request->image->getClientOriginalExtension();
		$imageNameTmb 	     = $imgName . '-thumb' . '.' . $request->image->getClientOriginalExtension();

        $fullUrlPath         = $urlPath . DIRECTORY_SEPARATOR . $imageName;
		$fullUrlPathTmb      = $urlPath . DIRECTORY_SEPARATOR . $imageNameTmb;

        $fullabsolutePath    = $absolutePath . DIRECTORY_SEPARATOR . $imageName;
		$fullabsoluteTmbPath = $absolutePath . DIRECTORY_SEPARATOR . $imageNameTmb;

		$background 		= Image::canvas($width, $height);

		// but keep aspect-ratio and do not size up,
		$img = Image::make($request->file('image')->getRealPath())->fit($width, $height, function ($c) {
		    $c->aspectRatio();
		    // $c->upsize();
		});
		
		// insert resized image centered into background
		$background->insert($img, 'center');

		// la marca de agua
		// $background->insert('images/sales/watermark.png');

		// save or do whatever you like
		$background->save($fullabsolutePath);

        // Make Thumbnail
        $backgroundTbm = Image::canvas($tmbWidth, $tmbHeight);

        $imgTbm = Image::make($request->file('image')->getRealPath())->fit($tmbWidth, $tmbHeight, function ($c) {
            $c->aspectRatio();
            // $c->upsize();
        });
        
        // insert resized image centered into background
        $backgroundTbm->insert($imgTbm, 'center');

        // la marca de agua
        // $background->insert('images/sales/watermark.png');

        // save or do whatever you like
        $backgroundTbm->save($fullabsoluteTmbPath);

        return $fullUrlPath;
    	
    }

    public function deleteImage(Request $request){

    	if($request->has('url')){

            $path     = public_path() . $request->url;
    		$pathTbm  = public_path() . $request->thumbnail;

    		if(File::exists($path)){
    			
                File::delete($path);
    			File::delete($pathTbm);
    			return 'Jevi';
    		}
    	}
    	
    	return response('Not Found',412);
    }


    public function categories(Request $request){

        if($request->ajax()){

            $categories  = DB::collection('blog_setup')->first();

            return ['rows' => $categories['categories']];
        }
        return view('admin.blog.categories');
    }

    public function storeCategory(Request $request){

        $validator = \Validator::make($request->all(), [
            'title'     => 'required|max:180',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

        $result = DB::collection('blog_setup.categories')
                        ->push(['title' => $request->title, 'slug' => str_slug($request->title)]);

        return ['title' => $request->title, 'slug' => str_slug($request->title)];
    }

    public function updateCategory(Request $request){

        $validator = \Validator::make($request->all(), [
            'title'     => 'required|max:180',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),412);
        }

        $result = DB::collection('blog_setup')
                        ->where('categories.slug', $request->old)
                        ->update(['$set' => ['categories.$' => ['title' => $request->title, 'slug' => str_slug($request->title)]]]);

        // Update all blog Posts
        $blogU = DB::collection('blog')
            ->where('category_slug', $request->old)
            ->update(['category' => $request->title, 'category_slug' => str_slug($request->title)]);

        if($result){

            return ['title' => $request->title, 'slug' => str_slug($request->title)];
        }

        return response($request->old,412);
    }


    public function deleteCategory(Request $request){

        $result = DB::collection('blog_categories')->where('_id', $request->id['$oid'])->detele();

        return json_encode(true);
    }

    public function updateComment(Request $request){

        $result = DB::collection('blog')
            ->where('comments.id', new \MongoDB\BSON\ObjectID($request->id))
            ->update(['$set' => ['comments.$' => [
		                                            'id'            => new \MongoDB\BSON\ObjectID($request->id),
		                                            'created_at'    => $request->created_at,
		                                            'content'       => $request->content,
		                                            'author'        => $request->author,
		                                            'spam'          => $request->spam
		                                        ]]]);

        return $result;
    }





}
