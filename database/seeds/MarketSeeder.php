<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Market Setup

        DB::collection('market_setup')->insert([
            [
                'currencies'    => [['title' => 'RD'], ['title' => 'US']],
                'statues'       => [['title' => 'Queue'], ['title' => 'Published'], ['title' => 'Sold']],
                'categories'    => [
                    ['slug' => 'audio'                  ,'title' => 'Audio'                   ],
                    ['slug' => 'otros'                  ,'title' => 'Otros'                   ],
                    ['slug' => 'frenos'                 ,'title' => 'Frenos'                  ],
                    ['slug' => 'enfriamiento'           ,'title' => 'Enfriamiento'            ],
                    ['slug' => 'transmision'            ,'title' => 'Transmision'             ],
                    ['slug' => 'electronica'            ,'title' => 'Electrónica'             ],
                    ['slug' => 'motor'                  ,'title' => 'Motor'                   ],
                    ['slug' => 'escape'                 ,'title' => 'Escape'                  ],
                    ['slug' => 'exterior'               ,'title' => 'Exterior'                ],
                    ['slug' => 'fluidos'                ,'title' => 'Fluidos'                 ],
                    ['slug' => 'inducción-forzada'      ,'title' => 'Inducción Forzada'       ],
                    ['slug' => 'combustible-y-encendido','title' => 'Combustible y Encendido' ],
                    ['slug' => 'garaje-y-seguridad'     ,'title' => 'Garaje y Seguridad'      ],
                    ['slug' => 'medidores'              ,'title' => 'Medidores'               ],
                    ['slug' => 'entrada-de-aire'        ,'title' => 'Entrada de aire'         ],
                    ['slug' => 'interior'               ,'title' => 'Interior'                ],
                    ['slug' => 'suspension'             ,'title' => 'Suspensión'              ],
                    ['slug' => 'aros'                   ,'title' => 'Aros'                    ],
                    ['slug' => 'gomas'                  ,'title' => 'Gomas'                   ],
                    ['slug' => 'accesorios'             ,'title' => 'Accesorios'              ],
                    ['slug' => 'luces'                  ,'title' => 'Luces'                   ],
                    ['slug' => 'vehiculo'               ,'title' => 'Vehiculo'                ]
                ]
            ]
        ]);

        // Market insert test
        $faker          = Faker::create();

        $setup          = DB::collection('market_setup')->first();
        $statues        = $setup['statues'];
        $currencies     = $setup['currencies'];
        $categories     = $setup['categories'];
        $images         = [];
        
        for ($i=0; $i < rand(1,5); $i++) { 
            array_push($images, ['url' => 'http://fakeimg.pl/342x200/?text=Henkoh&font=lobster', 'thumbnail' => ($i ==0)? true : false]);
        }

        for ($i=0; $i < 200; $i++) { 

            DB::collection('market')->insert([
                'title'     => $faker->firstName,
                'slug'      => str_slug($faker->firstName),
                'user_id'   => Henkoh\User::all()->random()->id,
                'status'    => $statues[rand(0, count($statues) -1)]['title'],
                'category'  => $categories[rand(0, count($categories) -1)]['title'],
                'content'   => $faker->sentence(30),
                'currency'  => $currencies[rand(0, count($currencies) -1)]['title'],
                'price'     => 100,
                'views'     => rand(0,1000),
                'token'     => str_slug(str_random(40)),
                'images'    => $images,
                'tags'      => ['motor','gtr','skyline'],
                'created_at'=> date('Y-m-d H:i:s')
            ]);
        } 
    }
}
