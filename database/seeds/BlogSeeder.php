<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Blog_config
        DB::collection('blog_setup')->insert([
            [
                'statues' => [
                    ['title' => 'Queue'],
                    ['title' => 'Published']
                ],
                'categories' => [
                    ['slug' => 'drift'     ,'title' => 'Drift'    ],
                    ['slug' => 'drag'      ,'title' => 'Drag'     ],
                    ['slug' => 'circuito'  ,'title' => 'Circuito' ],
                    ['slug' => 'calle'     ,'title' => 'Calle'    ]
                ]
            ]
        ]); 

        // Blog insert test
        $faker          = Faker::create();

        $setup          = DB::collection('blog_setup')->first();
        $statues        = $setup['statues'];
        $categories     = $setup['categories'];

		$image 		    = [
			'url' 		=> 'http://fakeimg.pl/868x200/?text=Henkoh&font=lobster', 
			'thumbnail' => 'http://fakeimg.pl/200x200/?text=Henkoh&font=lobster'
		];        

        for ($i=0; $i < 100; $i++) { 

            $category = $categories[rand(0, count($categories) -1)]['title'];

            DB::collection('blog')->insert([
                'title'             => $faker->firstName,
                'slug'              => str_slug($faker->firstName),
                'user_id'           => Henkoh\User::all()->random()->id,
                'status'            => $statues[rand(0, count($statues) -1)]['title'],
                'category'          => $category,
                'category_slug'     => str_slug($category),
                'content'           => $faker->sentence(30),
                'views'             => rand(0,1000),
                'image' 	        => $image,
                'tags'              => ['motor','gtr','skyline'],
                'created_at'        => date('Y-m-d H:i:s'),
                'comments'          => [
                    [
                        'id'            => new \MongoDB\BSON\ObjectID(),
                        'created_at'    => date('Y-m-d H:i:s'),
                        'author'        => $faker->firstName,
                        'user_id'       => Henkoh\User::all()->random()->id,
                        'content'       => $faker->sentence(10),
                        'spam'          => true
                    ],
                    [
                        'id'            => new \MongoDB\BSON\ObjectID(),
                        'created_at'    => date('Y-m-d H:i:s'),
                        'author'        => $faker->firstName,
                        'user_id'       => Henkoh\User::all()->random()->id,
                        'content'       => $faker->sentence(10),
                        'spam'          => false
                    ],
                    
                ]
            ]);
        } 
    }
}
