@extends('admin.layout.app')

@section('title', 'Dashboard')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <admin-blog></admin-blog>
        </div>
    </div>
@endsection
