@extends('admin.layout.app')

@section('title', 'Dashboard')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <admin-blog-categories></admin-blog-categories>
        </div>
    </div>
@endsection
