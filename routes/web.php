<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/register', 				'Auth\RegisterController@register');
Route::get('/confirm-email', 			'Auth\RegisterController@confirmEmail');
Route::get('/confirm-token/{token}', 	'Auth\RegisterController@confirmToken');
Route::auth();

Route::group(['prefix' => 'admin', 'middleware' => ['auth','role:administrator']], function(){
	Route::get('/', 						'AdminController@dashboard')->name('admin-dashboard');

	// Market
	Route::get('/market', 					'MarketController@index');
	Route::post('/market', 					'MarketController@store');
	Route::put('/market/', 					'MarketController@update');
	Route::post('/market/image', 			'MarketController@saveImages');
	Route::delete('/market/image', 			'MarketController@deleteImage');
	Route::delete('/market/{id}', 			'MarketController@delete');

	// Blog
	Route::get('/blog', 					'BlogController@index');
	Route::post('/blog', 					'BlogController@store');
	Route::put('/blog/', 					'BlogController@update');
	Route::post('/blog/image', 				'BlogController@saveImages');
	Route::delete('/blog/image', 			'BlogController@deleteImage');
	Route::delete('/blog/{id}', 			'BlogController@delete');

	// Comments
	Route::put('/blog/update-comment', 		'BlogController@updateComment');

	// Categories
	Route::get('/blog/category', 			'BlogController@categories');
	Route::post('/blog/category', 			'BlogController@storeCategory');
	Route::put('/blog/category/', 			'BlogController@updateCategory');
	Route::delete('/blog/category/{title}', 'BlogController@deleteCategory');
});

// Market
Route::group(['prefix' => 'market'], function(){

	Route::get('/', 						'Front\MarketController@index');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/unauthorized', function(){

	return 'pa donde va manin';
});


