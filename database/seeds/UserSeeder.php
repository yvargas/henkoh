<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user 			    = new Henkoh\User;
        $user->name         = 'Admin';
        $user->slug_name    = str_slug('Admin');
        $user->email 	    = 'admin@henkoh.com';
        $user->password     = bcrypt('configuracion00H');
        $user->phone 	    = '8299830333';
        $user->roles 	    = ['administrator'];
        $user->save();
    }
}
