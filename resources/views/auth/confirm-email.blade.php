@extends('front.layout.lite')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInUp">
        <div>        
            <div>
                <h1 class="logo-name">.</h1>
            </div>
            <h3>Henkoh</h3>
            <p>Hemos enviado un correo a tu bandeja de entrada, sigue los pasos para completar el registro. </p>
        </div>
    </div>
@endsection
