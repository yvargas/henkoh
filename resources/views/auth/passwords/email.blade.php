@extends('front.layout.lite')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>        
            <div>
                <h1 class="logo-name">.</h1>
            </div>
            <h3>Reset Password</h3>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form class="m-t" method="POST" role="form" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                
                <button type="submit" class="btn btn-primary block full-width m-b">Send Password Reset Link</button>

            </form>
            <p class="m-t"> <small>Henkoh 2015 - {{ date('Y') }}</small> </p>
        </div>
    </div>
@endsection
