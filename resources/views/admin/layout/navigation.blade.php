<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <!-- <img alt="image" class="img-circle " src="{{ asset('/images/profiles/profile.jpg') }}"> -->
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">User</strong>
                     </span> <span class="text-muted text-xs block">Role <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Mailbox</a></li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Log Out
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    H
                </div>
            </li>
            <li>
                <li class="">
                    <a href="/admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
                </li>

                <li class="">
                    <a href="/admin/market"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Market</span></a>
                </li>

                <li class="">
                    <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Blog</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class=""><a href="/admin/blog">Posts</a></li>
                        <li class=""><a href="/admin/blog/category">Categories</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-car"></i> <span class="nav-label">Workshop</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                        <li><a href="#">Create</a></li>
                        <li><a href="#">Pending</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-bookmark"></i> <span class="nav-label">Deals</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                        <li><a href="#">Create</a></li>
                        <li><a href="#">Pending</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Events</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                        <li><a href="#">Create</a></li>
                        <li><a href="#">Pending</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Foro</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-star"></i> <span class="nav-label">Versus</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                        <li><a href="#">Create</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label">Employ</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">List</a></li>
                        <li><a href="#">Create</a></li>
                    </ul>
                </li>

                <li class="">
                    <a href="#"><i class="fa fa-gear"></i> <span class="nav-label">Settings</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class=""><a href="/admin/settings"></i> Users</a></li>
                        <li class=""><a href="/admin/settings"></i> General</a></li>
                        <li class=""><a href="/admin/settings"></i> Ads</a></li>
                    </ul>
                </li>
            </li>
        </ul> 
    </div>
</nav>
