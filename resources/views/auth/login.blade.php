@extends('front.layout.lite')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>        
            <div>
                <h1 class="logo-name">.</h1>
            </div>
            <h3>Welcome to Henkoh</h3>
            <form class="m-t" method="POST" role="form" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="{{ route('password.request') }}"><small>Olvidaste tu password?</small></a>
                <p><br></p>
                <a class="btn btn-sm btn-white btn-block" href="/register">Registrarme</a>
            </form>
            <p class="m-t"> <small>Henkoh 2015 - {{ date('Y') }}</small> </p>
        </div>
    </div>
@endsection
