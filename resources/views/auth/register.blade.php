@extends('front.layout.lite')

@section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>        
            <div>
                <h1 class="logo-name">.</h1>
            </div>
            <h3>Welcome to Henkoh</h3>
            <p> desc </p>
            <p>Login in. To see it in action.</p>
            <form class="m-t" method="POST" role="form" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" placeholder="Username" required="" name="name">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" placeholder="Email" required="" name="email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Password" required="" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Confirm Password" required="" name="password_confirmation">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Registrarme</button>

                <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
            </form>
            <p class="m-t"> <small>Henkoh 2015 - {{ date('Y') }}</small> </p>
        </div>
    </div>
@endsection
