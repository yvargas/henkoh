const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 						'public/js')
mix.js('resources/js/admin.js', 					'public/js')
   .sass('resources/sass/app.scss', 				'public/css');

mix.copy('resources/images/', 						'public/images/');
mix.copy('resources/vendor/font-awesome/fonts',  	'public/fonts');


mix.styles([
    'resources/vendor/font-awesome/css/font-awesome.css',
    'resources/vendor/animate/animate.css',
], 'public/css/components.css');

mix.scripts([
    'resources/vendor/metisMenu/jquery.metisMenu.js',
    'resources/vendor/slimscroll/jquery.slimscroll.min.js',
    'resources/vendor/pace/pace.min.js',
], 'public/js/components.js');
