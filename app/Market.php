<?php

namespace Henkoh;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Market extends Eloquent
{
	protected $collection = 'market';

	public function user()
    {
        return $this->belongsTo('Henkoh\User');
    }
    
}
