<?php

namespace Henkoh\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!in_array($role, \Auth::user()->roles)){
            return redirect('/unauthorized');
        }

        return $next($request);
    }
}
