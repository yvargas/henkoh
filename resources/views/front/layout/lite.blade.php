<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>@yield('title') </title>
    <link rel="shortcut icon"           href="{{ asset('images/henkoh.ico') }}" type="image/x-icon" />
    <meta name="viewport"               content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token"             content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible"  content="IE=edge"/>
    <meta name="HandheldFriendly"       content="True">
    <meta name="MobileOptimized"        content="320">
    <meta name="referrer"               content="origin">
    <meta name="viewport"               content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">    
    <meta name="author"                 content="Henkoh"/>
    <link rel="stylesheet"              href="{{ asset('css/app.css') }}" />
    <link rel="stylesheet"              href="{{ asset('css/components.css') }}" />
    @section('css')
    @show
</head>
<body>
    <div id="henkoh">
            
        <!-- Main view  -->
        @yield('content')

    </div>
    
    <!-- End wrapper-->
    <!-- <script src="{{ asset('js/app.js') }}" type="text/javascript"></script> -->
    <!-- <script src="{{ asset('js/components.js') }}" type="text/javascript"></script> -->
    @section('scripts')
@show
</body>
</html>